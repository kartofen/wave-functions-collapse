### Wave Function Collapse

This an implementation of very primitive tile-based wave function collapse.
It uses tiles to generate an image, similar to this:

![Demo 0 of the program](demo/demo0.png) ![Demo 1 of the program](demo/demo1.png)

It can use colored tileset like the one included in the files directory, which is copied from
Mxgmn's Wave Function Collapse README.

There is a script to turn the generated images  into png using imagemagick.
All files including the tiles are saved in the files directory.

### Build

To build use `./build.sh`
To build and run use `./build.sh run`
To clean use `./build.sh clean`

Two files will be build in the bin directory,
`bin/gen_tiles` and  `bin/wfc`

I am pretty sure the `bin/wfc` should work on Windows,
`bin/gen_tiles` uses linux-only header to
create the directories for the files (files and files/tiles)
and uses `cp` to copy files around.

It would be easy to make everything work on windows,
I just dont want to take my time and make it cross platform.

And I dont want to use Makefile, because I dont like it,
the project is not too big, a shell script is enough.

### How it works

Firstly `bin/wfc --help` can be used for some info on program arguments

The main program `bin/wfc` uses the tiles in `files/tiles`, which are ppm files.
There is a file called `tiles.dat` in the same directory that holds information about the tiles, and it enables `bin/wfc` to generate the tile rotations

The `bin/gen_tiles` generates the `tiles.dat` file and optionally the tiles themselves.
There is a file called `config.h` under the src directory which is used to specify things about
`tiles.dat` and basically configuring `bin/gen_tiles`. More info in the file as comments.

To use a tileset different generated one it is explained in `src/config.h`.
For a tilset to be compatable with this project, it needs to be all ppm files,
and files should be named exactly like the names in the table in `src/config.h`.
For example see the tileset in `files/tilesets/knots`.

### Limitations

1. A tile can either not connect with a tile or connect with all tiles that have a connection on the given direction.
   It cant connect only with specific tiles.
2. Only tile based wave function collapse, I dont understand the other more complex one

### References

[Mxgmn's Wave Function Collapse README](https://github.com/mxgmn/WaveFunctionCollapse)
