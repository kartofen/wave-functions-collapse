#!/bin/sh

cd ${0%/*} # go to project root

FLAGS="-Wall -Wextra -g -pedantic"
SRCD="src"
ODIR="obj"
BIN="bin"
VALGRND=""
RUN=0

function __run__ {
    RUN=1
}

function __leak__ {
    VALGRND="valgrind --leak-check=full --show-leak-kinds=all -s"
    RUN=1
}

function __clean__ {
    rm -rf $BIN
    rm -rf $ODIR
    rm -rf files/tiles
    rm -f  files/file*
    kill $( ps -q $$ -o pgid= ) # exit
}

set -xe

if ! { [[ $# -eq 0 ]]; } 2> /dev/null
then
    __$1__
fi

mkdir -p $ODIR
mkdir -p $BIN

gcc -c $SRCD/gen_tiles.c -o $ODIR/gen_tiles.o $FLAGS
gcc -c $SRCD/ppm.c       -o $ODIR/ppm.o       $FLAGS
gcc -c $SRCD/tilemap.c   -o $ODIR/tilemap.o   $FLAGS
gcc -c $SRCD/tiles.c     -o $ODIR/tiles.o     $FLAGS
gcc -c $SRCD/main.c      -o $ODIR/main.o      $FLAGS

gcc -o $BIN/wfc $ODIR/main.o $ODIR/ppm.o $ODIR/tiles.o $ODIR/tilemap.o $FLAGS
gcc -o $BIN/gen_tiles $ODIR/gen_tiles.o $ODIR/ppm.o $FLAGS

if ! { [[ $RUN -eq 0 ]]; } 2> /dev/null
then
    $VALGRND $BIN/gen_tiles
    $VALGRND $BIN/wfc
fi
