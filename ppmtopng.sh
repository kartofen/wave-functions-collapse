#!/bin/sh

cd ${0%/*} # go to project root
set -xe

for i in files/file*.ppm
do
    pnmtopng "$i" >> "${i%.*}.png"
done
