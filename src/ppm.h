#ifndef PPM_H
#define PPM_H

void save_as_ppm(char* file_path, small_t *t, size_t width, size_t height, size_t scaler);

char *load_from_ppm(char *file_path, size_t *width, size_t *height);

void free_ppm(char *img);

#endif
