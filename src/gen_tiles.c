#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include "typedef.h"
#include "ppm.h"
#include "config.h"

#define TILE_TYPES  5
#define TILE_WIDTH  3
#define TILE_HEIGHT 3

// possible types: X T I
char symetry[TILE_TYPES] = "XTIIT";

char tile_names[TILE_TYPES][7] = {
    "blank", "t", "plus", "line", "corner"
};

small_t tiles[TILE_TYPES][TILE_WIDTH*TILE_HEIGHT] = {
    {
      0, 0, 0,
      0, 0, 0,
      0, 0, 0
    }, {
      0, 0, 0,
      1, 1, 1,
      0, 1, 0
    }, {
      0, 1, 0,
      1, 1, 1,
      0, 1, 0
    },{
      0, 0, 0,
      1, 1, 1,
      0, 0, 0
    }, {
      0, 1, 0,
      0, 1, 1,
      0, 0, 0
    }
};

small_t tiles_connections[TILE_TYPES][4] = {
    { 0, 0, 0, 0 },
    { 0, 1, 1, 1 },
    { 1, 1, 1, 1 },
    { 0, 1, 1, 0 },
    { 1, 1, 0, 0 },
};

void set_color(small_t *pixels, small_t *t, int x, int y, char k, char blank, char connection)
{
    pixels[(y * TILE_WIDTH + x)*3 + k] = (t[y * TILE_WIDTH + x] == 0) ? blank : connection;

}
FILE *fp;

void save(small_t *t, small_t *c, int n, int i)
{
#ifdef GENERATE_PPM_TILES
    (void)i;

    char file_name[64] = {0};

    sprintf(file_name, "files/tiles/tile_%d.ppm", n);

    small_t *pixels = malloc(TILE_WIDTH * TILE_HEIGHT *3);
    for(int y = 0; y < TILE_HEIGHT; y++)
        for(int x = 0; x < TILE_WIDTH; x++) {
            set_color(pixels, t, x, y, 0,
                      generated_tile_colors[0][0],
                      generated_tile_colors[1][0]);
            set_color(pixels, t, x, y, 1,
                      generated_tile_colors[0][1],
                      generated_tile_colors[1][1]);
            set_color(pixels, t, x, y, 2,
                      generated_tile_colors[0][2],
                      generated_tile_colors[1][2]);
        }

    save_as_ppm(file_name, pixels, TILE_WIDTH, TILE_HEIGHT, 1);
    free(pixels);
    printf("Saved file: %s\n", file_name);
#endif
#ifdef TILESET_NAME
    (void)t; (void)c;

    char command[128];
    sprintf(command, "cp files/tilesets/%s/%s.ppm files/tiles/tile_%d.ppm", TILESET_NAME, tile_names[i], n);
    system(command);
#endif

    small_t connections = 0;
    for(int ci = 0; ci < 4; ci++)
    {
        connections |= (c[ci] << (3-ci));
    }

    fwrite(&connections, sizeof(connections), 1, fp);
}

void gen()
{
    mkdir("files", S_IRWXU | S_IRGRP | S_IXGRP | S_IROTH | S_IXOTH);
    mkdir("files/tiles", S_IRWXU | S_IRGRP | S_IXGRP | S_IROTH | S_IXOTH);

    fp = fopen("files/tiles/tiles.dat", "wb");
    if(!fp) {
        fprintf(stderr, "ERROR: Could not open file files/tiles/tiles.dat");
        exit(EXIT_FAILURE);
    }

    size_t tiles_sz = sizeof(tiles_to_load)/sizeof(int);
    fwrite(&tiles_sz, sizeof(size_t), 1, fp);

    char syms[TILE_TYPES];
    size_t sym_sz = 0;

    for(size_t n = 0; n < tiles_sz; n++)
    {
        int i = tiles_to_load[n];
        save(tiles[i], tiles_connections[i], n, i);
        syms[sym_sz++] = symetry[i];
    }

    fwrite(syms, 1, sym_sz, fp);

    fclose(fp);
}


int main(void)
{
    gen();
    return 0;
}
