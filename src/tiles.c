#include <stdio.h>
#include <stdlib.h>
#include "typedef.h"
#include "tiles.h"
#include "ppm.h"

extern size_t TILES;

extern size_t TILE_WIDTH;
extern size_t TILE_HEIGHT;

small_t *(tiles[TILES_CAP]);
small_t tile_connections[TILES_CAP];

static int rotate(int x, int y, int r, int w)
{
    switch(r) {
    case 0: // no roatate
        return y * w + x;
    case 1: // right
        return (w*w - w) + y - (x * w);
    case 2: // left
        return (w-1) - y + (x * w);
    case 3: // down
        return (w*w-1) - (y * w) - x;
    }

    return -1;
}

static void rotate_tiles(char *t, int i, int r)
{
    tiles[i] = malloc(TILE_WIDTH * TILE_HEIGHT * 3);

    for(size_t y = 0; y < TILE_HEIGHT; y++)
        for(size_t x = 0; x < TILE_WIDTH; x++)
            for(int k = 0; k < 3; k++)
                tiles[i][(y * TILE_WIDTH + x)*3 + k] = t[(rotate(x, y, r, TILE_WIDTH))*3 + k];
}

static void rotate_connections(char *tc, int i, int n, int r)
{
    tile_connections[n] = 0;
    for(int y = 0; y < 2; y++)
        for(int x = 0; x < 2; x++)
            tile_connections[n] |= (((tc[i] >> rotate(x, y, r, 2)) & 1) << (y * 2 + x));
}

static void load_tile_data(char *tc, char *ts, size_t *tid)
{
    char file_path[32] = "files/tiles/tiles.dat";
    FILE *fp = fopen(file_path, "rb");
    if(!fp) {
        fprintf(stderr, "Could not open file: %s\n", file_path);
        exit(EXIT_FAILURE);
    }

    fread(tid, sizeof(size_t), 1, fp);
    fread(tc, 1, *tid, fp);
    fread(ts, 1, *tid, fp);

    fclose(fp);
}

void load_tiles()
{
    size_t tid = 0;
    char tile_con[TILES_CAP];
    char tile_sym[TILES_CAP];
    load_tile_data(tile_con, tile_sym, &tid);

    if(tid == 0) {
        fprintf(stderr, "ERROR: No tiles could be loaded\n");
        exit(EXIT_FAILURE);
    }

    size_t n = 0;
    for(size_t i = 0; i < tid; i++, n++)
    {
        char file_path[32];
        sprintf(file_path, "files/tiles/tile_%ld.ppm", i);
        char *t = load_from_ppm(file_path, &TILE_WIDTH, &TILE_HEIGHT);

        switch(tile_sym[i])
        {
        case 'X':
            rotate_tiles(t, n, 0);
            rotate_connections(tile_con, i, n, 0);
            break;
        case 'T':
            for(int j = 0; j < 4; j++) {
                rotate_tiles(t, n, j);
                rotate_connections(tile_con, i, n, j);
                n++;
             } n--;
            break;
        case 'I':
            rotate_tiles(t, n, 0);
            rotate_connections(tile_con, i, n, 0);
            n++;
            rotate_tiles(t, n, 1);
            rotate_connections(tile_con, i, n, 1);
        }

        free_ppm(t);
    }

    TILES = n;

    if(TILES > TILES_CAP) {
        fprintf(stderr, "ERROR: Too many tiles: %ld\n", TILES);
        exit(EXIT_FAILURE);
    }
}

void free_tiles()
{
    for(size_t i = 0; i < TILES; i++)
        free(tiles[i]);
}

void print_tiles()
{
    for(size_t i = 0; i < TILES; i++)
    {
        printf("Tile: %ld\n", i);
        printf("Connections: %b\n", tile_connections[i]);
        for(size_t y = 0; y < TILE_HEIGHT; y++)
        {
            for(size_t x = 0; x < TILE_WIDTH; x++)
                putchar((tiles[i][(y * TILE_WIDTH + x)*3 + 0]) == 0 ? '#' : '.');
            putchar('\n');
        }
        putchar('\n');
    }
}

int get_tile_pixel(size_t t, size_t x, size_t y, int k)
{
    return tiles[t][(y * TILE_WIDTH + x)*3 + k];
}

small_t *get_tile_connections()
{
    return tile_connections;
}
