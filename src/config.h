// comment this if you dont want to generate the tiles, but
// want to use the tileset defined in the TILESET_NAME directive
// #define GENERATE_PPM_TILES

#ifdef GENERATE_PPM_TILES
    int generated_tile_colors[2][3] = {
        { 0,   32,  63  }, // blank     | default 255
        { 173, 239, 209 }  // not blank | defalut 0
        // { 255, 255, 255 },
        // { 0,   0,   0   }
    };
#endif


// name of the tileset, localated in files/tilesets directory
// must be defined only when GENERATE_PPM_TILES is not
// if both arent defined, only tiles.dat will be generated
#define TILESET_NAME "knots"

/* The TILES are

--------------------------------------------------
|  0: ...  blank      |  1: ...  t (down)        |
|     ...  X symetry  |     ###  T symetry       |
|     ...             |     .#.                  |
|  -------------------+--------------------------|
|  2: .#.  plus       |  3: ...  line            |
|     ###  I symetry  |     ###  I symetry       |
|     .#.             |     ...                  |
|  -------------------+--------------------------|
|  4: .#.  corner     |                          |
|     .##  T symetry  |                          |
|     ...             |                          |
--------------------------------------------------

The number are the tile indexes, the values that you
need to put in the tiles_to_load array

 */

// Array for which tiles to include the tiles.dat
int tiles_to_load[] = {
    2,
    3,
    4,
};
