#ifndef TILES_H
#define TILES_H

void load_tiles();
void free_tiles();

void print_tiles();

int get_tile_pixel(size_t t, size_t x, size_t y, int k);
small_t *get_tile_connections();

#endif
