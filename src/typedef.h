#ifndef TYPEDEF_H
#define TYPEDEF_H

// useful definitions

#define TILES_CAP 128
#define TILEMAP_CAP 16384

#include <stdint.h>
#include <stddef.h>
typedef unsigned char small_t;
typedef uint64_t big_t;

#endif
