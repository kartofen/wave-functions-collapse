#ifndef TILEMAP_H
#define TILEMAP_H

// set bit n in tile t in tilemap
void set(int t, int n);
// is bit n set in tile t in tilemap
int is_set(int t, int n);

// self explanatory names
int has_collapsed(int t);
int get_collapsed_tile(int t);
void collapse(int t, int n);
size_t count_entropy(int t);

void init_tilemap();
void destroy_tilemap();

void generate_tile_masks(small_t* tile_connections);

// applly a mask m, r (from tile_masks)
// to tile t in tilemap
void mask(int t, int m, int r);



#endif
